# Security Guard Map Scheduler --non release

This is a code example of the Security Guard Map Scheduler for Golden Voice. (a few versions previous)

This Laravel web app is a scheduler for security guards on location at Coachella events. 

A pin can be dropped on the leaflet map (generic map used initially but to be replaced with custom tiles from mapserver)

The app gathers info on Companies, events, guards, and guard attributes or levels such as hourly pay rate double time etc.

The app uses datatables to render html tables of schedule rows fully searchable and filterable.

The project page allows for companies to be selected from global aggregate for an event then corresponding guards and attributes for those companies. (place a checkbox in corresponding row)

In the map view a guard can be dropped and it's lat lng on the map as well as other info will be updated to the db.

Laravel, Mysql, Datatables, Oracle Yajra, Leaflet, Laratrust, Sass, Reliese Laravel (Jeffrey Way), Jquery, Ajax

1.5 months work

I was given a sql script from a previous version (I did not have access to see or any description of)
Much of the table structure had to be modified as reflected in my migrations.

Future plans to build the layout from scratch instead of using the suggested admin template.

I had to decide to use datatables instead of the jquery/bootgrid styled, utilized and suggested by the admin template, better for forming the payload from the controllers for the html table to be rendered/re-rendered.

Plan to refactor using vue.js instead of the admin template styling and jquery objects.

I set up a staging server on Digital Ocean to demo for the client.

Client requested project on pause while gathering resources and review of further requirements.

Screenshots and Flow
[Get Image Screenshots in zip](https://bitbucket.org/ranfouprodev/security-guard-map-scheduler-non-release/downloads/screenshots_scheduler.zip)

How it works:



