<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\Guardlevel;
use App\Models\Guard;
use App\Models\Company;

class AdminContentController extends Controller
{
    //
    public function admin()
    {
        //return view('admin-content');
        return view('event')->with('events', Event::all());
    }

    public function event()
    {
        return view('event')->with('events', Event::all());
    }

    public function company()
    {
        //dd(Company::all());
        return view('company')->with('companies', Company::all('id','Name','Description'));
    }

    public function guards()
    {
        return view('guards')->with('levels' , Guardlevel::all())->with('companies', Company::all('id','Name','Description'));
    }

    public function levels()
    {
        //dd(Guardlevel::all());
        return view('levels')->with('levels' , Guardlevel::all())->with('companies', Company::all());
    }

    public function accounts()
    {
        return view('accounts');
    }

    public function settings()
    {
        return view('settings');
    }
    
}
