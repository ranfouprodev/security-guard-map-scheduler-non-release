<?php

namespace App\Http\Controllers;
use App\Models\Company;
use App\Models\CompanyEvent;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;

class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //dd($request->all());

        Company::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return Company::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //dd($request->all());

        $record = new Company;
        $company = $record->find($request->editRecord);
        
        $company['Description'] = $request->Description;
        $company['Name'] = $request->Name;
    
        $company->save();
        //return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Company::destroy($id);

        //add cascade where clauses.
    }

    public function event($id,$event)
    {
        $companyEvent = new CompanyEvent;
        $companyEvent ['event_id'] = $event;
        $companyEvent ['company_id'] = $id;
        $companyEvent->save();
    }

    public function eventremove($id,$event)
    {
        CompanyEvent::where(
            [
                ['company_id', '=', $id],
                ['event_id', '=', $event]
            ])->delete();
    }

    public function all()
    {
        $company = Company::all();
        
        return Datatables::of($company)
        ->with('events', CompanyEvent::all())
        ->addColumn('checkbox', function ($company) {   
            return '<div class="checkbox">
            <label>
                <input type="checkbox" onclick="javascript:checkboxClick(' . $company->id .', this);" data-id="' . $company->id . '" data-event="" class="add">
                <i class="input-helper"></i>
            </label>
        </div>';
          })
        ->addColumn('action', function($company){
            return "<a onclick=editForm('$company->id') class=\"edit btn btn-default btn-sm\"><span class=\"zmdi zmdi-edit zmdi-hc-fw\"></span></a> " .
                    "<a onclick=deleteData('$company->id') class=\"delete btn btn-default btn-sm\"><span class=\"zmdi zmdi-delete zmdi-hc-fw\"></span></a>";
        })->make(true);
    }
}
