<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Event;
use Yajra\Datatables\Datatables;

class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // dd($request->all());
       $event = new Event;
       $event['Name'] = $request->Name;
       $event['CenterLat'] = $request->CenterLat;
       $event['CenterLng'] = $request->CenterLng;
       $event['MapFolder'] = $request->MapFolder;
       
        $event->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return Event::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //dd($request->all());
        $record = new Event;
        $event = $record->find($request->editRecord);
        

        
        $event['Name'] = $request->Name;
        $event['CenterLat'] = $request->CenterLat;
        $event['CenterLng'] = $request->CenterLng;
        $event['MapFolder'] = $request->MapFolder;

        
        $event->save();
        //return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Event::destroy($id);
        //return redirect()->back();
    }
    /*
    * @return \Illuminate\Http\Response
    */
    public function all()
    {
        
        /*$data = '{
            "current": 1,
            "rowCount": 10,
            "rows": [
              {
                "id": 19,
                "sender": "123@test.de",
                "received": "2014-05-30T22:15:00"
              },
              {
                "id": 14,
                "sender": "123@test.de",
                "received": "2014-05-30T20:15:00"
              },
              ...
            ],
            "total": 1123
          }';*/
          //return [Event::all()];

          $event = Event::all();

        return Datatables::of($event)
        ->addColumn('action', function($event){
            return "<a onclick=editForm('$event->id') class=\"edit btn btn-default btn-sm\"><span class=\"zmdi zmdi-edit zmdi-hc-fw\"></span></a> " .
                    "<a onclick=deleteData('$event->id') class=\"delete btn btn-default btn-sm\"><span class=\"zmdi zmdi-delete zmdi-hc-fw\"></span></a>";
        })->make(true);

    }
}
