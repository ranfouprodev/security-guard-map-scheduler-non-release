<?php

namespace App\Http\Controllers;

use App\Models\Guard;
use App\Models\Guardlevel;
use App\Models\Company;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class GuardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd($request->all());

        $guard = array();
        
        $guard['Callsign'] = $request->Callsign;
        $guard['level_id'] = $request->level_id;
        $guard['company_id'] = $request->company_id;
        
        
        Guard::create($guard);
       // return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Guard  $guard
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $levelid = Guard::find($id)->level_id;
        $companyid = Guard::find($id)->company_id;

        $levelDescription = GuardLevel::find($levelid)->Description;
        $companyName = Company::find($companyid)->Name;

        $guard = Guard::find($id);

        $extra = array();
        $extra['levelDescription'] = $levelDescription;
        $extra['companyName'] = $companyName;

        //$guard->push($levelDescription);

        //$guard->push($companyName);

        return [$guard,$extra];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Guard  $guard
     * @return \Illuminate\Http\Response
     */
    public function edit(Guard $guard)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Guard  $guard
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        //dd($request->all());
        $record = new Guard;
        $guard = $record->find($request->editRecord);
        
        $guard['Callsign'] = $request->Callsign;
        $guard['level_id'] = $request->level_id;
        $guard['company_id'] = $request->company_id;

        $guard->save();
        //return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Guard  $guard
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       Guard::destroy($id);
    }
    public function all()
    {
        //$guardarr = array();
        //$guard = array();
        $guard = Guard::all();

        /*foreach($myguards as $theguard)
        {
            $guardarr['id'] = $theguard->id;
            $guardarr['callsign'] = $theguard->Callsign;
            $guardarr['level'] = Guardlevel::find($theguard->Level)->Description;
            $guardarr['company'] = Company::find($theguard->company_id)->Name;
            //array_push($guard, $guardarr);
        }*/

        //dd($guard['id']);
        return Datatables::of($guard)
        ->addColumn('checkbox', function ($guard) {
            return '<div class="checkbox">
            <label>
                <input type="checkbox" onclick="javascript:checkboxClick(' .$guard->id .');" data-id="' . $guard->id . '" id="add" value="add">
                <i class="input-helper"></i>
            </label>
        </div>';
          })
        ->addColumn('levelDescription', function($guard){

            return Guardlevel::find($guard->level_id)->Description;

        })->addColumn('companyName', function($guard){

            return Company::find($guard->company_id)->Name;

        })->addColumn('action', function($guard){
            return "<a onclick=editForm($guard->id) class=\"edit btn btn-default btn-sm\"><span class=\"zmdi zmdi-edit zmdi-hc-fw\"></span></a> " .
                    "<a onclick=deleteData($guard->id) class=\"delete btn btn-default btn-sm\"><span class=\"zmdi zmdi-delete zmdi-hc-fw\"></span></a>";
        })->make(true);
    }
}
