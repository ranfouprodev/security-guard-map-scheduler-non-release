<?php

namespace App\Http\Controllers;

use App\Models\Guardlevel;
use App\Models\Company;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class GuardlevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $level = array();
        $level['company_id'] = $request->company_id;
        $level['Description'] = $request->Description;
        $level['HourlyRate'] = $request->HourlyRate;
        $level['OvertimeRateFactor'] = $request->OvertimeRateFactor;
        $level['DoubleTimeRateFactor'] = $request->DoubleTimeRateFactor;
        Guardlevel::create($level);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Guardlevel  $guardlevel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $level = GuardLevel::find($id);
        $companyName = Company::find($level->company_id)->Name;

        return [$level, $companyName];
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Guardlevel  $guardlevel
     * @return \Illuminate\Http\Response
     */
    public function edit(Guardlevel $guardlevel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Guardlevel  $guardlevel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Guardlevel $guardlevel)
    {
        //
        //dd($request->all());
        $record = new Guardlevel;
        $level = $record->find($request->editRecord);
        

        $level['company_id'] = $request->company_id;
        $level['Description'] = $request->Description;
        $level['HourlyRate'] = $request->HourlyRate;
        $level['OvertimeRateFactor'] = $request->OvertimeRateFactor;
        $level['DoubleTimeRateFactor'] = $request->DoubleTimeRateFactor;

        
        $level->save();
        //return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Guardlevel  $guardlevel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Guardlevel::destroy($id);
    }
    
    public function all()
    {
        $level = Guardlevel::all();

        return Datatables::of($level)
        ->addColumn('checkbox', function ($level) {
            return '<div class="checkbox">
            <label>
                <input type="checkbox" onclick="javascript:checkboxClick(' .$level->id .');" data-id="' . $level->id . '"  id="add" value="add">
                <i class="input-helper"></i>
            </label>
        </div>';
          })
        ->addColumn('action', function($level){
            return "<a onclick=editForm('$level->id') class=\"edit btn btn-default btn-sm\"><span class=\"zmdi zmdi-edit zmdi-hc-fw\"></span></a> " .
                    "<a onclick=deleteData('$level->id') class=\"delete btn btn-default btn-sm\"><span class=\"zmdi zmdi-delete zmdi-hc-fw\"></span></a>";
        })->addColumn('companyName', function($level){

            return Company::find($level->company_id)->Name;

        })->make(true);
    }
}
