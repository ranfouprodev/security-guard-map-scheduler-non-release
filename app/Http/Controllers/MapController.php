<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mapper;
use App\Models\Event;
use App\Models\Guard;
use App\Models\Guardlevel;
use App\Models\Company;


class MapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       


    public function index($id)
    {
    
        Mapper::map(34.0141860, -118.2878860, ['zoom' => 18]); // center geo for FYF 2017 event

        $current = array();
        $current['id'] = $id;
        $current['name']= Event::find($id)->Name;

        return view('map')->with('menu', Event::all('id','Name'))->with('current', $current)->with('guards', Guard::all())->with('levels', Guardlevel::all())->with('companies', Company::all()); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $schedule = new Schedule;
        
        $schedule->Date = $request->Date;
        $schedule->Location = $request->Location;
        $schedule->Company_id = $request->company_id;
        $schedule->Guard_id = $request->guard_id;
        $schedule->Level_id = $request->level_id;
        $schedule->PositionDescription = $request->PositionDescription;
        $schedule->StartTime = $request->StartTime;
        $schedule->EndTime = $request->EndTime;
        $schedule->Event_id = $request->event_id;

        //dd($schedule);
        $schedule->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $id passed in is the event_id for this event

        // possibly get center lat lng for event

        /*get all of the lat/lngs and icos(paths) from schedule model for this event
            get all staff types from schedule table
            get the icons for the staff type

            create mapper for each with a path to this custom icon

        */
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
