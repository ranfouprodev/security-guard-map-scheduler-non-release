<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Guard;
use App\Models\Guardlevel;
use App\Models\Company;
use App\Models\Event;
use App\Models\Schedule;


class ProjectContentController extends Controller
{
    //
    public function schedule($id)
    {
        $current = array();
        $current['id'] = $id;
        $current['Name']= Event::find($id)->Name;
       
        return view('schedule')->with('menu', Event::all('id','Name'))->with('current', $current)->with('guards', Guard::all())->with('levels', Guardlevel::all())->with('companies', Company::all()); 
    }

    public function company()
    {
        //dd(Company::all());
        return view('project_company')->with('companies', Company::all());
    }

    public function guards()
    {
        return view('project_guards')->with('levels' , Guardlevel::all())->with('companies', Company::all());
    }

    public function levels()
    {
        //dd(Guardlevel::all());
        return view('project_levels')->with('levels' , Guardlevel::all())->with('companies', Company::all());
    }
}
