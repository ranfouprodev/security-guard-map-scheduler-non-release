<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\Guard;
use App\Models\Guardlevel;
use App\Models\Company;
use App\Models\Schedule;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;

class TableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index($id){
        // todo -- add logic to check for 0 and manage in modals and table view.
        
        $current = array();
        $current['id'] = $id;
        $current['Name']= Event::find($id)->Name;
       
        return view('table')->with('menu', Event::all('id','Name'))->with('current', $current)->with('guards', Guard::all())->with('levels', Guardlevel::all())->with('companies', Company::all()); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());

        $schedule = new Schedule;
        
        $schedule->Date = $request->Date;
        $schedule->Location = $request->Location;
        $schedule->company_id = $request->company_id;
        $schedule->guard_id = $request->guard_id;
        $schedule->level_id = $request->level_id;
        $schedule->PositionDescription = $request->PositionDescription;
        $schedule->StartTime = $request->StartTime;
        $schedule->EndTime = $request->EndTime;
        $schedule->event_id = $request->event_id;
        $schedule->Lat = $request->Lat;
        $schedule->Lng = $request->Lng;


        //dd($schedule);
        $schedule->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 
        $schedule = Schedule::find($id);
        $companyName = Company::find($schedule->company_id)->Name;
        $Callsign = Guard::find($schedule->guard_id)->Callsign;
        $Level = Guardlevel::find($schedule->level_id)->Description;
        return [$schedule, $companyName, $Callsign, $Level];  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //dd($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //dd($request->all());
        $record = new schedule;
        $sched = $record->find($request->editRecord);
        $sched->Date = $request->Date;
        $sched->Location = $request->Location;
        $sched->company_id = $request->company_id;
        $sched->guard_id = $request->guard_id;
        $sched->level_id = $request->level_id;
        $sched->PositionDescription = $request->PositionDescription;
        $sched->StartTime = $request->StartTime;
        $sched->EndTime = $request->EndTime;
        $sched->event_id = $request->event_id;
  
        $sched->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Schedule::destroy($id);
    }

    public function all($id)
    {
        // has to be by event id  
        $schedule = Schedule::where('event_id', $id)->get();
    
        return Datatables::of($schedule)
        ->addColumn('action', function($schedule){
            return "<a onclick=editForm('$schedule->id') class=\"edit btn btn-default btn-sm\"><span class=\"zmdi zmdi-edit zmdi-hc-fw\"></span></a> " .
                    "<a onclick=deleteData('$schedule->id') class=\"delete btn btn-default btn-sm\"><span class=\"zmdi zmdi-delete zmdi-hc-fw\"></span></a>";
        })->addColumn('companyName', function($schedule){

            return Company::find($schedule->company_id)->Name;

        })->addColumn('Callsign', function($schedule){

            return Guard::find($schedule->guard_id)->Callsign;

        })->addColumn('Level', function($schedule){

            return Guardlevel::find($schedule->level_id)->Description;

        })->make(true);
    }
}
