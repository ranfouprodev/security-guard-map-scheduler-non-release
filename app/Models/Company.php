<?php

/**
 * Created by Reliese Model ..
 * Date: Wed, 26 Dec 2018 01:35:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Company
 * 
 * @property int $ID
 * @property int $EventID
 * @property string $Name
 * 
 * @property \App\Models\Event $event
 * @property \Illuminate\Database\Eloquent\Collection $company_skills
 * @property \Illuminate\Database\Eloquent\Collection $user_permissions
 *
 * @package App\Models
 */
class Company extends Eloquent
{
	protected $primaryKey = 'id';
	public $timestamps = false;

	/*protected $casts = [
		'EventID' => 'int'
	];*/

	protected $fillable = [
		'Name',
		'Description'
	];
	
	public function level()
    {
        return $this->belongsTo('App\Models\Guardlevel');
	}

	public function events(){
		return $this->belongsToMany('App\Models\Event');
	}

}
