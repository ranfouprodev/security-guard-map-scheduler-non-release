<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 01:35:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CompanySkill
 * 
 * @property int $ID
 * @property int $IconTemplateID
 * @property int $CompanyID
 * @property string $Name
 * @property float $RegularRate
 * @property float $OvertimeRate
 * @property float $OvertimeThreshold
 * @property float $DoubleTimeRate
 * @property float $DoubleTimeThreshold
 * 
 * @property \App\Models\Company $company
 * @property \App\Models\IconTemplate $icon_template
 * @property \Illuminate\Database\Eloquent\Collection $icon_placements
 *
 * @package App\Models
 */
class CompanySkill extends Eloquent
{
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'IconTemplateID' => 'int',
		'CompanyID' => 'int',
		'RegularRate' => 'float',
		'OvertimeRate' => 'float',
		'OvertimeThreshold' => 'float',
		'DoubleTimeRate' => 'float',
		'DoubleTimeThreshold' => 'float'
	];

	protected $fillable = [
		'IconTemplateID',
		'CompanyID',
		'Name',
		'RegularRate',
		'OvertimeRate',
		'OvertimeThreshold',
		'DoubleTimeRate',
		'DoubleTimeThreshold'
	];

	public function company()
	{
		return $this->belongsTo(\App\Models\Company::class, 'CompanyID'); // company has many companySkills
	}

	public function icon_template()
	{
		return $this->belongsTo(\App\Models\IconTemplate::class, 'IconTemplateID');
	}

	public function icon_placements()
	{
		return $this->hasMany(\App\Models\IconPlacement::class, 'CompanySkillID');
	}
}
