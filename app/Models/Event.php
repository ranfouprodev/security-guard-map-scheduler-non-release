<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 01:35:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Event
 * 
 * @property int $ID
 * @property string $Name
 * @property float $CenterLat
 * @property float $CenterLng
 * @property string $MapFolder
 * 
 * @property \Illuminate\Database\Eloquent\Collection $companies
 * @property \Illuminate\Database\Eloquent\Collection $user_permissions
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Event extends Eloquent
{
	protected $primaryKey = 'id';
	public $timestamps = false;

	protected $casts = [
		'CenterLat' => 'float',
		'CenterLng' => 'float'
	];

	protected $fillable = [
		'Name',
		'CenterLat',
		'CenterLng',
		'MapFolder'
	];

	public function companies(){
		return $this->belongsToMany('App\Models\Company');
	}

	public function levels(){
		return $this->belongsToMany('App\Models\Guardlevel');
	}

	public function guards(){
		return $this->belongsToMany('App\Models\Guard');
	}
	
}
