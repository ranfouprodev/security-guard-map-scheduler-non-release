<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Guard extends Model
{
    //
    protected $fillable = [
        'Callsign',
        'level_id',
        'company_id',	
	];

    public function levels()
    {
        return $this->hasMany('App\Models\Guardlevel');
    }

    public function companies()
    {
        return $this->hasMany('App\Models\Company');
    }
    
    public function events(){
		return $this->belongsToMany('App\Models\Event');
	}

}