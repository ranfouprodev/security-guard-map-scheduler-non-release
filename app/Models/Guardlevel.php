<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Guardlevel extends Model
{

    protected $casts = [
		'HourlyRate' => 'float',
        'OvertimeRateFactor' => 'float',
        'DoubleTimeRateFactor' => 'float'
		];
    //
    protected $fillable = [
		'company_id',	
		'Description',
		'HourlyRate',
		'OvertimeRateFactor',
		'DoubleTimeRateFactor'
	];

		public function companies()
    {
        return $this->hasMany('App\Models\Company');
		}

		public function events(){
			return $this->belongsToMany('App\Models\Event');
		}




}
