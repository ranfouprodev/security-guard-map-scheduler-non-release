<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 01:35:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class IconPlacement
 * 
 * @property int $ID
 * @property int $CompanySkillID
 * @property float $Lat
 * @property float $Lng
 * @property string $Description
 * @property string $Callsign
 * @property int $ParentPlacementID
 * 
 * @property \App\Models\CompanySkill $company_skill
 * @property \App\Models\IconPlacement $icon_placement
 * @property \Illuminate\Database\Eloquent\Collection $icon_placements
 * @property \Illuminate\Database\Eloquent\Collection $shifts
 *
 * @package App\Models
 */
class IconPlacement extends Eloquent
{
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'CompanySkillID' => 'int',
		'Lat' => 'float',
		'Lng' => 'float',
		'ParentPlacementID' => 'int'
	];

	protected $fillable = [
		'CompanySkillID',
		'Lat',
		'Lng',
		'Description',
		'Callsign',
		'ParentPlacementID'
	];

	public function company_skill()
	{
		return $this->belongsTo(\App\Models\CompanySkill::class, 'CompanySkillID');
	}

	public function icon_placement()
	{
		return $this->belongsTo(\App\Models\IconPlacement::class, 'ParentPlacementID'); 
	}

	public function icon_placements()
	{
		return $this->hasMany(\App\Models\IconPlacement::class, 'ParentPlacementID');
	}

	public function shifts()
	{
		return $this->hasMany(\App\Models\Shift::class, 'IconPlacementID');
	}
}
