<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 01:35:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class IconTemplate
 * 
 * @property int $ID
 * @property string $Path
 * @property string $GroupType
 * @property string $Name
 * @property string $Prefix
 * 
 * @property \Illuminate\Database\Eloquent\Collection $company_skills
 *
 * @package App\Models
 */
class IconTemplate extends Eloquent
{
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'Path',
		'GroupType',
		'Name',
		'Prefix'
	];

	public function company_skills()
	{
		return $this->hasMany(\App\Models\CompanySkill::class, 'IconTemplateID');
	}
}
