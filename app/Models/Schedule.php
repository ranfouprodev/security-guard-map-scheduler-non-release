<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
	protected $primaryKey = 'id';
	
	protected $casts = [
		//'Date' => 'datetime:yyyy/mm/dd'
	];


    protected $fillable = [
		//'Date',
		'Location',
		'Company_id',
		'Guard_id',
		'Level_id',
		'PositionDescription',
		'StartTime',
		'EndTime',
		'event_id'
	];

	
}
