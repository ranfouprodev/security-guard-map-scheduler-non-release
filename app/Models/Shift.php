<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 01:35:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Shift
 * 
 * @property int $ID
 * @property int $SortOrder
 * @property int $IconPlacementID
 * @property \Carbon\Carbon $ShiftDate
 * @property \Carbon\Carbon $StartTime
 * @property \Carbon\Carbon $EndTime
 * @property int $Enabled
 * 
 * @property \App\Models\IconPlacement $icon_placement
 *
 * @package App\Models
 */
class Shift extends Eloquent
{
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'SortOrder' => 'int',
		'IconPlacementID' => 'int',
		'Enabled' => 'int'
	];

	protected $dates = [
		'ShiftDate',
		'StartTime',
		'EndTime'
	];

	protected $fillable = [
		'SortOrder',
		'IconPlacementID',
		'ShiftDate',
		'StartTime',
		'EndTime',
		'Enabled'
	];

	public function icon_placement() //guard placement
	{
		return $this->belongsTo(\App\Models\IconPlacement::class, 'IconPlacementID');
	}
}
