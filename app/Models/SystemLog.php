<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 01:35:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SystemLog
 * 
 * @property int $OrderLogID
 * @property string $TableName
 * @property string $Action
 * @property int $UserID
 * @property int $TableKeyID
 * @property string $OldData
 * @property string $NewData
 * @property \Carbon\Carbon $ActionDate
 *
 * @package App\Models
 */
class SystemLog extends Eloquent
{
	protected $primaryKey = 'OrderLogID';
	public $timestamps = false;

	protected $casts = [
		'UserID' => 'int',
		'TableKeyID' => 'int'
	];

	protected $dates = [
		'ActionDate'
	];

	protected $fillable = [
		'TableName',
		'Action',
		'UserID',
		'TableKeyID',
		'OldData',
		'NewData',
		'ActionDate'
	];
}
