<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Dec 2018 01:35:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserPermission
 * 
 * @property int $ID
 * @property int $EventID
 * @property int $UserID
 * @property int $CompanyID
 * @property string $AccessLevel
 * 
 * @property \App\Models\Company $company
 * @property \App\Models\Event $event
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class UserPermission extends Eloquent
{
	protected $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'EventID' => 'int',
		'UserID' => 'int',
		'CompanyID' => 'int'
	];

	protected $fillable = [
		'EventID',
		'UserID',
		'CompanyID',
		'AccessLevel'
	];

	public function company()
	{
		return $this->belongsTo(\App\Models\Company::class, 'CompanyID');
	}

	public function event()
	{
		return $this->belongsTo(\App\Models\Event::class, 'EventID');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'UserID');
	}
}
