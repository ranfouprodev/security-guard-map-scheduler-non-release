<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('events', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('Name')->nullable()->unique('UIDX_events_Name');
			$table->float('CenterLat', 11, 7)->default(0.0000000);
			$table->float('CenterLng', 11, 7)->default(0.0000000);
			$table->string('MapFolder', 100)->default('maptiles');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('events');
	}

}
