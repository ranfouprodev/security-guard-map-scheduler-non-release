<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
           // $table->engine = 'InnoDB';
            $table->increments('id');
            $table->date('Date');
            $table->string('Location');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->integer('guard_id')->unsigned();
            $table->foreign('guard_id')->references('id')->on('guards');
            $table->integer('level_id')->unsigned();
            $table->foreign('level_id')->references('id')->on('guardlevels');
            $table->string('PositionDescription');
            $table->string('StartTime',15);
			$table->string('EndTime',15);
            $table->integer('event_id')->unsigned();
            $table->foreign('event_id')->references('id')->on('events');
            $table->float('Lat', 11, 7)->nullable();
			$table->float('Lng', 11, 7)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        Schema::dropIfExists('schedules');
    }
}
