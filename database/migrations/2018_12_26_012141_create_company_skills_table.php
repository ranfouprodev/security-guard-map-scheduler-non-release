<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompanySkillsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('company_skills', function(Blueprint $table)
		{
			$table->increments('ID');
			$table->integer('IconTemplateID')->unsigned()->index('FK__company_skills_Icons'); 
			$table->integer('CompanyID')->unsigned()->index('FK__company_skills_Companies');
			$table->string('Name'); // guards name
			$table->float('RegularRate', 10, 6);
			$table->float('OvertimeRate', 10, 6);
			$table->float('OvertimeThreshold', 10, 6);
			$table->float('DoubleTimeRate', 10, 6);
			$table->float('DoubleTimeThreshold', 10, 6);
			$table->unique(['Name','CompanyID'], 'UIDX_company_skills_Name');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('company_skills');
	}

}
