<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIconPlacementsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('icon_placements', function(Blueprint $table)
		{
			$table->increments('ID');
			$table->integer('CompanySkillID')->unsigned()->index('FK__icon_placements_CompanySkill');
			$table->float('Lat', 10, 6);
			$table->float('Lng', 10, 6);
			$table->string('Description', 100);
			$table->string('Callsign', 100);
			$table->integer('ParentPlacementID')->unsigned()->nullable()->index('FK__icon_placements_ParentPlacementID');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('icon_placements');
	}

}
