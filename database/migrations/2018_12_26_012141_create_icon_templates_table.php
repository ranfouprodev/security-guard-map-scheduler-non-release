<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIconTemplatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('icon_templates', function(Blueprint $table)
		{
			$table->increments('ID');
			$table->string('Path')->nullable();
			$table->enum('GroupType', array('Manager','Supervisor','Post'));
			$table->string('Name')->nullable();
			$table->string('Prefix', 10)->default('');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('icon_templates');
	}

}
