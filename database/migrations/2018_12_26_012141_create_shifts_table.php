<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShiftsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('shifts', function(Blueprint $table)
		{
			$table->increments('ID');
			$table->integer('SortOrder');
			$table->integer('IconPlacementID')->unsigned();
			$table->date('ShiftDate');
			$table->time('StartTime');
			$table->time('EndTime');
			$table->integer('Enabled');
			$table->unique(['IconPlacementID','ShiftDate','SortOrder'], 'UIDX_shifts_IconPlacement_SortOrder');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('shifts');
	}

}
