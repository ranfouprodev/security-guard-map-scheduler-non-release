<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSystemLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('system_logs', function(Blueprint $table)
		{
			$table->increments('OrderLogID');
			$table->string('TableName', 50)->default('');
			$table->enum('Action', array('UPDATE','DELETE','INSERT'));
			$table->integer('UserID');
			$table->integer('TableKeyID');
			$table->text('OldData', 65535);
			$table->text('NewData', 65535);
			$table->timestamp('ActionDate')->default(DB::raw('CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('system_logs');
	}

}
