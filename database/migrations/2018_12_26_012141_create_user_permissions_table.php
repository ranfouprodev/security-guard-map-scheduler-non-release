<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserPermissionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_permissions', function(Blueprint $table)
		{
			$table->increments('ID');
			$table->integer('EventID')->unsigned()->index('FK__user_permissions_EventID');
			$table->integer('UserID')->unsigned()->index('FK__user_permissions_UserID');
			$table->integer('CompanyID')->unsigned()->nullable()->index('FK__user_permissions_CompanyID');
			$table->enum('AccessLevel', array('Readonly','Full','Admin'))->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_permissions');
	}

}
