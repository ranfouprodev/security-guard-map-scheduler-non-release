<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCompanySkillsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('company_skills', function(Blueprint $table)
		{
			$table->foreign('CompanyID', 'FK__company_skills_Companies')->references('ID')->on('companies')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('IconTemplateID', 'FK__company_skills_Icons')->references('ID')->on('icon_templates')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('company_skills', function(Blueprint $table)
		{
			$table->dropForeign('FK__company_skills_Companies');
			$table->dropForeign('FK__company_skills_Icons');
		});
	}

}
