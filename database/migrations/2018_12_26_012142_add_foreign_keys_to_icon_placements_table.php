<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIconPlacementsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('icon_placements', function(Blueprint $table)
		{
			$table->foreign('CompanySkillID', 'FK__icon_placements_CompanySkill')->references('ID')->on('company_skills')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('ParentPlacementID', 'FK__icon_placements_ParentPlacementID')->references('ID')->on('icon_placements')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('icon_placements', function(Blueprint $table)
		{
			$table->dropForeign('FK__icon_placements_CompanySkill');
			$table->dropForeign('FK__icon_placements_ParentPlacementID');
		});
	}

}
