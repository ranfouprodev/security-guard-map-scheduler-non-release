<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUserPermissionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_permissions', function(Blueprint $table)
		{
			$table->foreign('CompanyID', 'FK__user_permissions_CompanyID')->references('ID')->on('companies')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('EventID', 'FK__user_permissions_EventID')->references('ID')->on('events')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('UserID', 'FK__user_permissions_UserID')->references('ID')->on('users')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_permissions', function(Blueprint $table)
		{
			$table->dropForeign('FK__user_permissions_CompanyID');
			$table->dropForeign('FK__user_permissions_EventID');
			$table->dropForeign('FK__user_permissions_UserID');
		});
	}

}
