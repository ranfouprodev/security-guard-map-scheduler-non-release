CREATE DATABASE /*!32312 IF NOT EXISTS*/ `gv-scheduler`;
USE `gv-scheduler` ;

DROP TABLE IF EXISTS shifts;
DROP TABLE IF EXISTS icon_placements;
DROP TABLE IF EXISTS `company_skills`;
DROP TABLE IF EXISTS `icon_templates`;
DROP TABLE IF EXISTS `users`;
DROP TABLE IF EXISTS `companies`;
DROP TABLE IF EXISTS `events`;

CREATE TABLE `events` (
	ID INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Name VARCHAR(255),
	UNIQUE INDEX `UIDX_events_Name` (`Name`)
);

CREATE TABLE `companies` (
	ID INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	EventID INT UNSIGNED NOT NULL,
	Name VARCHAR(255),
	UNIQUE INDEX `UIDX_companies_Name` (`Name`),
	CONSTRAINT `FK__Event_Companies` FOREIGN KEY (`EventID`) REFERENCES `events` (`ID`) ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE `icon_templates` (
	ID INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Path VARCHAR(255),
	GroupType ENUM('Manager', 'Supervisor', 'Post') NOT NULL,
	Name VARCHAR(255)
);

INSERT INTO icon_templates(Path, GroupType, Name) VALUES
	('ico/man_bike.png', 'Manager', 'Man. Bike'),
	('ico/man_horse.png', 'Manager', 'Man. Horse'),
	('ico/man_feet.png', 'Manager', 'Man. Post'),
	('ico/sup_bike.png', 'Supervisor', 'Sup. Bike'),
	('ico/sup_horse.png', 'Supervisor', 'Sup. Horse'),
	('ico/sup_feet.png', 'Supervisor', 'Sup. Post'),
	('ico/cs_bike.png', 'Post', 'Post Bike'),
	('ico/cs_horse.png', 'Post', 'Post Horse'),
	('ico/cs_feet.png', 'Post', 'Post');
	
	
CREATE TABLE users (
	ID INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Password_Hash VARCHAR(255) NOT NULL,
	Username VARCHAR(255) NOT NULL,
	UNIQUE INDEX `UIDX_users_Username` (`Username`)
);


CREATE TABLE `company_skills` (
	ID INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	IconTemplateID INT(10) UNSIGNED NOT NULL,
	CompanyID INT(10) UNSIGNED NOT NULL,
	Name VARCHAR(255) NOT NULL,
	RegularRate FLOAT( 10, 6 ) NOT NULL, 
	OvertimeRate FLOAT( 10, 6 ) NOT NULL, 
	OvertimeThreshold FLOAT( 10, 6 ) NOT NULL, 
	DoubleTimeRate FLOAT( 10, 6 ) NOT NULL,
	DoubleTimeThreshold FLOAT( 10, 6 ) NOT NULL, 
	UNIQUE INDEX `UIDX_company_skills_Name` (`Name`, `CompanyID`),
	CONSTRAINT `FK__company_skills_Icons` FOREIGN KEY (`IconTemplateID`) REFERENCES `icon_templates` (`ID`) ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT `FK__company_skills_Companies` FOREIGN KEY (`CompanyID`) REFERENCES `companies` (`ID`) ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE `icon_placements` (
	ID INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	CompanySkillID INT(10) UNSIGNED NOT NULL,
	Lat FLOAT( 10, 6 ) NOT NULL,
	Lng FLOAT( 10, 6 ) NOT NULL,
	Description VARCHAR(100) NOT NULL,
	Callsign VARCHAR(100) NOT NULL,
	ParentPlacementID INT(10) UNSIGNED NULL,

	CONSTRAINT `FK__icon_placements_CompanySkill` FOREIGN KEY (`CompanySkillID`) REFERENCES `company_skills` (`ID`) ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT `FK__icon_placements_ParentPlacementID` FOREIGN KEY (`ParentPlacementID`) REFERENCES `icon_placements` (`ID`) ON UPDATE NO ACTION ON DELETE NO ACTION
);



CREATE TABLE `shifts` (
	ID INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	SortOrder INT(5) NOT NULL,
	IconPlacementID INT(10) UNSIGNED NOT NULL,
	ShiftDate DATE NOT NULL,
	StartTime Time NOT NULL,
	EndTime Time NOT NULL,
	Enabled INT(5) NOT NULL,
	CONSTRAINT `FK__shifts_IconPlacement` FOREIGN KEY (`IconPlacementID`) REFERENCES `icon_placements` (`ID`) ON UPDATE NO ACTION ON DELETE NO ACTION,
	UNIQUE INDEX `UIDX_shifts_IconPlacement_SortOrder` (`IconPlacementID`, ShiftDate, `SortOrder`)
);



-- SAMPLE DATA
INSERT INTO events(Name) VALUES('Coachella 2017');
INSERT INTO companies(Name, EventID) VALUES('Test Company', 1);
INSERT INTO users(Username, Password_Hash) VALUES('cam', '$2y$10$U.uFwy0YIWTFneBr2izNLuAeeYEI7GlLu74Mu3KlUN/UTre1eh8M6');
INSERT INTO company_skills(IconTemplateID, CompanyID, Name, RegularRate, OvertimeRate, OvertimeThreshold, DoubleTimeRate, DoubleTimeThreshold)
SELECT ID, 1, Name, 30, 45, 8, 60, 12
FROM icon_templates;

alter table users add CurrentEventID int(10) UNSIGNED not null default 1;
alter table events add CenterLat float(11,7) not null default 0, add CenterLng float(11,7) not null default 0;
ALTER TABLE users ADD CONSTRAINT fk_users_current_event_id FOREIGN KEY (CurrentEventID) REFERENCES events(ID);
insert into events(ID, Name, CenterLat, CenterLng) VALUES(2, 'FYF 2017', 34.014186, -118.287886);
update users set CurrentEventID = 2;

ALTER TABLE `companies`
	DROP INDEX `UIDX_companies_Name`,
	ADD UNIQUE INDEX `UIDX_companies_Name` (`EventID`, `Name`);

INSERT INTO companies(EventID, Name) SELECT 2, Name FROM companies WHERE EventID = 1;

INSERT INTO company_skills(IconTemplateID, CompanyID, Name, RegularRate, OvertimeRate, OvertimeThreshold, DoubleTimeRate, DoubleTimeThreshold) 
SELECT 
cs.IconTemplateID, cn.ID, cs.Name, cs.RegularRate, cs.OvertimeRate, cs.OvertimeThreshold, cs.DoubleTimeRate, cs.DoubleTimeThreshold
FROM company_skills cs
INNER JOIN companies co ON co.ID = cs.CompanyID AND co.EventID = 1
INNER JOIN companies cn ON cn.Name = co.Name AND cn.EventID = 2;

/* Audit Tracking Changes */

CREATE TABLE `system_logs` (
	`OrderLogID` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`TableName` VARCHAR(50) NOT NULL DEFAULT '',
	`Action` ENUM('UPDATE','DELETE','INSERT') NOT NULL,
	`UserID` INT(10) NOT NULL,
	`TableKeyID` INT(10) NOT NULL,
	`OldData` TEXT NOT NULL,
	`NewData` TEXT NOT NULL,
	`ActionDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`OrderLogID`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;




delimiter //
drop trigger if exists ai_companies // CREATE TRIGGER ai_companies AFTER INSERT ON companies
		FOR EACH ROW
		BEGIN
			DECLARE newInfo text;
			DECLARE oldInfo text;
			SET oldInfo = '';
			SET newInfo = CONCAT('','ID=',IFNULL(NEW.ID, 'NULL'), '
','EventID=',IFNULL(NEW.EventID, 'NULL'), '
','Name=',IFNULL(NEW.Name, 'NULL'), '
'); INSERT INTO system_logs(Action,TableName,NewData,UserID,TableKeyID)
		VALUES('INSERT','companies', newInfo,IFNULL(@UserID,0),NEW.ID);
		END; //
drop trigger if exists au_companies // CREATE TRIGGER au_companies AFTER UPDATE ON companies
		FOR EACH ROW
		BEGIN
			DECLARE newInfo text;
			DECLARE oldInfo text;
			SET newInfo = '';
			SET oldInfo = '';IF NEW.ID != OLD.ID OR
				( NEW.ID IS NULL AND OLD.ID IS NOT NULL) OR
				( NEW.ID IS NOT NULL AND OLD.ID IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'ID=', IFNULL(NEW.ID, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'ID=', IFNULL(OLD.ID, 'NULL'), '
');
END IF;IF NEW.EventID != OLD.EventID OR
				( NEW.EventID IS NULL AND OLD.EventID IS NOT NULL) OR
				( NEW.EventID IS NOT NULL AND OLD.EventID IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'EventID=', IFNULL(NEW.EventID, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'EventID=', IFNULL(OLD.EventID, 'NULL'), '
');
END IF;IF NEW.Name != OLD.Name OR
				( NEW.Name IS NULL AND OLD.Name IS NOT NULL) OR
				( NEW.Name IS NOT NULL AND OLD.Name IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'Name=', IFNULL(NEW.Name, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'Name=', IFNULL(OLD.Name, 'NULL'), '
');
END IF;IF newInfo!='' THEN INSERT INTO system_logs(Action,TableName,NewData,OldData,UserID,TableKeyID) VALUES('UPDATE','companies', newInfo,oldInfo,IFNULL(@UserID,0),NEW.ID); END IF;
		END; //drop trigger if exists ad_companies // CREATE TRIGGER ad_companies AFTER DELETE ON companies
				FOR EACH ROW
				BEGIN
					INSERT INTO system_logs(Action,TableName,oldData,UserID,TableKeyID)
		VALUES('DELETE','companies', '',IFNULL(@UserID,0),OLD.ID);
		END; //drop trigger if exists ai_company_skills // CREATE TRIGGER ai_company_skills AFTER INSERT ON company_skills
		FOR EACH ROW
		BEGIN
			DECLARE newInfo text;
			DECLARE oldInfo text;
			SET oldInfo = '';
			SET newInfo = CONCAT('','ID=',IFNULL(NEW.ID, 'NULL'), '
','IconTemplateID=',IFNULL(NEW.IconTemplateID, 'NULL'), '
','CompanyID=',IFNULL(NEW.CompanyID, 'NULL'), '
','Name=',IFNULL(NEW.Name, 'NULL'), '
','RegularRate=',IFNULL(NEW.RegularRate, 'NULL'), '
','OvertimeRate=',IFNULL(NEW.OvertimeRate, 'NULL'), '
','OvertimeThreshold=',IFNULL(NEW.OvertimeThreshold, 'NULL'), '
','DoubleTimeRate=',IFNULL(NEW.DoubleTimeRate, 'NULL'), '
','DoubleTimeThreshold=',IFNULL(NEW.DoubleTimeThreshold, 'NULL'), '
'); INSERT INTO system_logs(Action,TableName,NewData,UserID,TableKeyID)
		VALUES('INSERT','company_skills', newInfo,IFNULL(@UserID,0),NEW.ID);
		END; //
drop trigger if exists au_company_skills // CREATE TRIGGER au_company_skills AFTER UPDATE ON company_skills
		FOR EACH ROW
		BEGIN
			DECLARE newInfo text;
			DECLARE oldInfo text;
			SET newInfo = '';
			SET oldInfo = '';IF NEW.ID != OLD.ID OR
				( NEW.ID IS NULL AND OLD.ID IS NOT NULL) OR
				( NEW.ID IS NOT NULL AND OLD.ID IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'ID=', IFNULL(NEW.ID, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'ID=', IFNULL(OLD.ID, 'NULL'), '
');
END IF;IF NEW.IconTemplateID != OLD.IconTemplateID OR
				( NEW.IconTemplateID IS NULL AND OLD.IconTemplateID IS NOT NULL) OR
				( NEW.IconTemplateID IS NOT NULL AND OLD.IconTemplateID IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'IconTemplateID=', IFNULL(NEW.IconTemplateID, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'IconTemplateID=', IFNULL(OLD.IconTemplateID, 'NULL'), '
');
END IF;IF NEW.CompanyID != OLD.CompanyID OR
				( NEW.CompanyID IS NULL AND OLD.CompanyID IS NOT NULL) OR
				( NEW.CompanyID IS NOT NULL AND OLD.CompanyID IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'CompanyID=', IFNULL(NEW.CompanyID, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'CompanyID=', IFNULL(OLD.CompanyID, 'NULL'), '
');
END IF;IF NEW.Name != OLD.Name OR
				( NEW.Name IS NULL AND OLD.Name IS NOT NULL) OR
				( NEW.Name IS NOT NULL AND OLD.Name IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'Name=', IFNULL(NEW.Name, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'Name=', IFNULL(OLD.Name, 'NULL'), '
');
END IF;IF NEW.RegularRate != OLD.RegularRate OR
				( NEW.RegularRate IS NULL AND OLD.RegularRate IS NOT NULL) OR
				( NEW.RegularRate IS NOT NULL AND OLD.RegularRate IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'RegularRate=', IFNULL(NEW.RegularRate, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'RegularRate=', IFNULL(OLD.RegularRate, 'NULL'), '
');
END IF;IF NEW.OvertimeRate != OLD.OvertimeRate OR
				( NEW.OvertimeRate IS NULL AND OLD.OvertimeRate IS NOT NULL) OR
				( NEW.OvertimeRate IS NOT NULL AND OLD.OvertimeRate IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'OvertimeRate=', IFNULL(NEW.OvertimeRate, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'OvertimeRate=', IFNULL(OLD.OvertimeRate, 'NULL'), '
');
END IF;IF NEW.OvertimeThreshold != OLD.OvertimeThreshold OR
				( NEW.OvertimeThreshold IS NULL AND OLD.OvertimeThreshold IS NOT NULL) OR
				( NEW.OvertimeThreshold IS NOT NULL AND OLD.OvertimeThreshold IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'OvertimeThreshold=', IFNULL(NEW.OvertimeThreshold, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'OvertimeThreshold=', IFNULL(OLD.OvertimeThreshold, 'NULL'), '
');
END IF;IF NEW.DoubleTimeRate != OLD.DoubleTimeRate OR
				( NEW.DoubleTimeRate IS NULL AND OLD.DoubleTimeRate IS NOT NULL) OR
				( NEW.DoubleTimeRate IS NOT NULL AND OLD.DoubleTimeRate IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'DoubleTimeRate=', IFNULL(NEW.DoubleTimeRate, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'DoubleTimeRate=', IFNULL(OLD.DoubleTimeRate, 'NULL'), '
');
END IF;IF NEW.DoubleTimeThreshold != OLD.DoubleTimeThreshold OR
				( NEW.DoubleTimeThreshold IS NULL AND OLD.DoubleTimeThreshold IS NOT NULL) OR
				( NEW.DoubleTimeThreshold IS NOT NULL AND OLD.DoubleTimeThreshold IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'DoubleTimeThreshold=', IFNULL(NEW.DoubleTimeThreshold, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'DoubleTimeThreshold=', IFNULL(OLD.DoubleTimeThreshold, 'NULL'), '
');
END IF;IF newInfo!='' THEN INSERT INTO system_logs(Action,TableName,NewData,OldData,UserID,TableKeyID) VALUES('UPDATE','company_skills', newInfo,oldInfo,IFNULL(@UserID,0),NEW.ID); END IF;
		END; //drop trigger if exists ad_company_skills // CREATE TRIGGER ad_company_skills AFTER DELETE ON company_skills
				FOR EACH ROW
				BEGIN
					INSERT INTO system_logs(Action,TableName,oldData,UserID,TableKeyID)
		VALUES('DELETE','company_skills', '',IFNULL(@UserID,0),OLD.ID);
		END; //drop trigger if exists ai_events // CREATE TRIGGER ai_events AFTER INSERT ON events
		FOR EACH ROW
		BEGIN
			DECLARE newInfo text;
			DECLARE oldInfo text;
			SET oldInfo = '';
			SET newInfo = CONCAT('','ID=',IFNULL(NEW.ID, 'NULL'), '
','Name=',IFNULL(NEW.Name, 'NULL'), '
','CenterLat=',IFNULL(NEW.CenterLat, 'NULL'), '
','CenterLng=',IFNULL(NEW.CenterLng, 'NULL'), '
'); INSERT INTO system_logs(Action,TableName,NewData,UserID,TableKeyID)
		VALUES('INSERT','events', newInfo,IFNULL(@UserID,0),NEW.ID);
		END; //
drop trigger if exists au_events // CREATE TRIGGER au_events AFTER UPDATE ON events
		FOR EACH ROW
		BEGIN
			DECLARE newInfo text;
			DECLARE oldInfo text;
			SET newInfo = '';
			SET oldInfo = '';IF NEW.ID != OLD.ID OR
				( NEW.ID IS NULL AND OLD.ID IS NOT NULL) OR
				( NEW.ID IS NOT NULL AND OLD.ID IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'ID=', IFNULL(NEW.ID, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'ID=', IFNULL(OLD.ID, 'NULL'), '
');
END IF;IF NEW.Name != OLD.Name OR
				( NEW.Name IS NULL AND OLD.Name IS NOT NULL) OR
				( NEW.Name IS NOT NULL AND OLD.Name IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'Name=', IFNULL(NEW.Name, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'Name=', IFNULL(OLD.Name, 'NULL'), '
');
END IF;IF NEW.CenterLat != OLD.CenterLat OR
				( NEW.CenterLat IS NULL AND OLD.CenterLat IS NOT NULL) OR
				( NEW.CenterLat IS NOT NULL AND OLD.CenterLat IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'CenterLat=', IFNULL(NEW.CenterLat, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'CenterLat=', IFNULL(OLD.CenterLat, 'NULL'), '
');
END IF;IF NEW.CenterLng != OLD.CenterLng OR
				( NEW.CenterLng IS NULL AND OLD.CenterLng IS NOT NULL) OR
				( NEW.CenterLng IS NOT NULL AND OLD.CenterLng IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'CenterLng=', IFNULL(NEW.CenterLng, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'CenterLng=', IFNULL(OLD.CenterLng, 'NULL'), '
');
END IF;IF newInfo!='' THEN INSERT INTO system_logs(Action,TableName,NewData,OldData,UserID,TableKeyID) VALUES('UPDATE','events', newInfo,oldInfo,IFNULL(@UserID,0),NEW.ID); END IF;
		END; //drop trigger if exists ad_events // CREATE TRIGGER ad_events AFTER DELETE ON events
				FOR EACH ROW
				BEGIN
					INSERT INTO system_logs(Action,TableName,oldData,UserID,TableKeyID)
		VALUES('DELETE','events', '',IFNULL(@UserID,0),OLD.ID);
		END; //drop trigger if exists ai_icon_placements // CREATE TRIGGER ai_icon_placements AFTER INSERT ON icon_placements
		FOR EACH ROW
		BEGIN
			DECLARE newInfo text;
			DECLARE oldInfo text;
			SET oldInfo = '';
			SET newInfo = CONCAT('','ID=',IFNULL(NEW.ID, 'NULL'), '
','CompanySkillID=',IFNULL(NEW.CompanySkillID, 'NULL'), '
','Lat=',IFNULL(NEW.Lat, 'NULL'), '
','Lng=',IFNULL(NEW.Lng, 'NULL'), '
','ParentPlacementID=',IFNULL(NEW.ParentPlacementID, 'NULL'), '
','Description=',IFNULL(NEW.Description, 'NULL'), '
','Callsign=',IFNULL(NEW.Callsign, 'NULL'), '
'); INSERT INTO system_logs(Action,TableName,NewData,UserID,TableKeyID)
		VALUES('INSERT','icon_placements', newInfo,IFNULL(@UserID,0),NEW.ID);
		END; //
drop trigger if exists au_icon_placements // CREATE TRIGGER au_icon_placements AFTER UPDATE ON icon_placements
		FOR EACH ROW
		BEGIN
			DECLARE newInfo text;
			DECLARE oldInfo text;
			SET newInfo = '';
			SET oldInfo = '';IF NEW.ID != OLD.ID OR
				( NEW.ID IS NULL AND OLD.ID IS NOT NULL) OR
				( NEW.ID IS NOT NULL AND OLD.ID IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'ID=', IFNULL(NEW.ID, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'ID=', IFNULL(OLD.ID, 'NULL'), '
');
END IF;IF NEW.CompanySkillID != OLD.CompanySkillID OR
				( NEW.CompanySkillID IS NULL AND OLD.CompanySkillID IS NOT NULL) OR
				( NEW.CompanySkillID IS NOT NULL AND OLD.CompanySkillID IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'CompanySkillID=', IFNULL(NEW.CompanySkillID, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'CompanySkillID=', IFNULL(OLD.CompanySkillID, 'NULL'), '
');
END IF;IF NEW.Lat != OLD.Lat OR
				( NEW.Lat IS NULL AND OLD.Lat IS NOT NULL) OR
				( NEW.Lat IS NOT NULL AND OLD.Lat IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'Lat=', IFNULL(NEW.Lat, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'Lat=', IFNULL(OLD.Lat, 'NULL'), '
');
END IF;IF NEW.Lng != OLD.Lng OR
				( NEW.Lng IS NULL AND OLD.Lng IS NOT NULL) OR
				( NEW.Lng IS NOT NULL AND OLD.Lng IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'Lng=', IFNULL(NEW.Lng, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'Lng=', IFNULL(OLD.Lng, 'NULL'), '
');
END IF;IF NEW.ParentPlacementID != OLD.ParentPlacementID OR
				( NEW.ParentPlacementID IS NULL AND OLD.ParentPlacementID IS NOT NULL) OR
				( NEW.ParentPlacementID IS NOT NULL AND OLD.ParentPlacementID IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'ParentPlacementID=', IFNULL(NEW.ParentPlacementID, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'ParentPlacementID=', IFNULL(OLD.ParentPlacementID, 'NULL'), '
');
END IF;IF NEW.Description != OLD.Description OR
				( NEW.Description IS NULL AND OLD.Description IS NOT NULL) OR
				( NEW.Description IS NOT NULL AND OLD.Description IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'Description=', IFNULL(NEW.Description, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'Description=', IFNULL(OLD.Description, 'NULL'), '
');
END IF;IF NEW.Callsign != OLD.Callsign OR
				( NEW.Callsign IS NULL AND OLD.Callsign IS NOT NULL) OR
				( NEW.Callsign IS NOT NULL AND OLD.Callsign IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'Callsign=', IFNULL(NEW.Callsign, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'Callsign=', IFNULL(OLD.Callsign, 'NULL'), '
');
END IF;IF newInfo!='' THEN INSERT INTO system_logs(Action,TableName,NewData,OldData,UserID,TableKeyID) VALUES('UPDATE','icon_placements', newInfo,oldInfo,IFNULL(@UserID,0),NEW.ID); END IF;
		END; //drop trigger if exists ad_icon_placements // CREATE TRIGGER ad_icon_placements AFTER DELETE ON icon_placements
				FOR EACH ROW
				BEGIN
					INSERT INTO system_logs(Action,TableName,oldData,UserID,TableKeyID)
		VALUES('DELETE','icon_placements', '',IFNULL(@UserID,0),OLD.ID);
		END; //drop trigger if exists ai_icon_templates // CREATE TRIGGER ai_icon_templates AFTER INSERT ON icon_templates
		FOR EACH ROW
		BEGIN
			DECLARE newInfo text;
			DECLARE oldInfo text;
			SET oldInfo = '';
			SET newInfo = CONCAT('','ID=',IFNULL(NEW.ID, 'NULL'), '
','Path=',IFNULL(NEW.Path, 'NULL'), '
','GroupType=',IFNULL(NEW.GroupType, 'NULL'), '
','Name=',IFNULL(NEW.Name, 'NULL'), '
'); INSERT INTO system_logs(Action,TableName,NewData,UserID,TableKeyID)
		VALUES('INSERT','icon_templates', newInfo,IFNULL(@UserID,0),NEW.ID);
		END; //
drop trigger if exists au_icon_templates // CREATE TRIGGER au_icon_templates AFTER UPDATE ON icon_templates
		FOR EACH ROW
		BEGIN
			DECLARE newInfo text;
			DECLARE oldInfo text;
			SET newInfo = '';
			SET oldInfo = '';IF NEW.ID != OLD.ID OR
				( NEW.ID IS NULL AND OLD.ID IS NOT NULL) OR
				( NEW.ID IS NOT NULL AND OLD.ID IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'ID=', IFNULL(NEW.ID, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'ID=', IFNULL(OLD.ID, 'NULL'), '
');
END IF;IF NEW.Path != OLD.Path OR
				( NEW.Path IS NULL AND OLD.Path IS NOT NULL) OR
				( NEW.Path IS NOT NULL AND OLD.Path IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'Path=', IFNULL(NEW.Path, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'Path=', IFNULL(OLD.Path, 'NULL'), '
');
END IF;IF NEW.GroupType != OLD.GroupType OR
				( NEW.GroupType IS NULL AND OLD.GroupType IS NOT NULL) OR
				( NEW.GroupType IS NOT NULL AND OLD.GroupType IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'GroupType=', IFNULL(NEW.GroupType, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'GroupType=', IFNULL(OLD.GroupType, 'NULL'), '
');
END IF;IF NEW.Name != OLD.Name OR
				( NEW.Name IS NULL AND OLD.Name IS NOT NULL) OR
				( NEW.Name IS NOT NULL AND OLD.Name IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'Name=', IFNULL(NEW.Name, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'Name=', IFNULL(OLD.Name, 'NULL'), '
');
END IF;IF newInfo!='' THEN INSERT INTO system_logs(Action,TableName,NewData,OldData,UserID,TableKeyID) VALUES('UPDATE','icon_templates', newInfo,oldInfo,IFNULL(@UserID,0),NEW.ID); END IF;
		END; //drop trigger if exists ad_icon_templates // CREATE TRIGGER ad_icon_templates AFTER DELETE ON icon_templates
				FOR EACH ROW
				BEGIN
					INSERT INTO system_logs(Action,TableName,oldData,UserID,TableKeyID)
		VALUES('DELETE','icon_templates', '',IFNULL(@UserID,0),OLD.ID);
		END; //drop trigger if exists ai_shifts // CREATE TRIGGER ai_shifts AFTER INSERT ON shifts
		FOR EACH ROW
		BEGIN
			DECLARE newInfo text;
			DECLARE oldInfo text;
			SET oldInfo = '';
			SET newInfo = CONCAT('','ID=',IFNULL(NEW.ID, 'NULL'), '
','SortOrder=',IFNULL(NEW.SortOrder, 'NULL'), '
','IconPlacementID=',IFNULL(NEW.IconPlacementID, 'NULL'), '
','ShiftDate=',IFNULL(NEW.ShiftDate, 'NULL'), '
','StartTime=',IFNULL(NEW.StartTime, 'NULL'), '
','EndTime=',IFNULL(NEW.EndTime, 'NULL'), '
','Enabled=',IFNULL(NEW.Enabled, 'NULL'), '
'); INSERT INTO system_logs(Action,TableName,NewData,UserID,TableKeyID)
		VALUES('INSERT','shifts', newInfo,IFNULL(@UserID,0),NEW.ID);
		END; //
drop trigger if exists au_shifts // CREATE TRIGGER au_shifts AFTER UPDATE ON shifts
		FOR EACH ROW
		BEGIN
			DECLARE newInfo text;
			DECLARE oldInfo text;
			SET newInfo = '';
			SET oldInfo = '';IF NEW.ID != OLD.ID OR
				( NEW.ID IS NULL AND OLD.ID IS NOT NULL) OR
				( NEW.ID IS NOT NULL AND OLD.ID IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'ID=', IFNULL(NEW.ID, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'ID=', IFNULL(OLD.ID, 'NULL'), '
');
END IF;IF NEW.SortOrder != OLD.SortOrder OR
				( NEW.SortOrder IS NULL AND OLD.SortOrder IS NOT NULL) OR
				( NEW.SortOrder IS NOT NULL AND OLD.SortOrder IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'SortOrder=', IFNULL(NEW.SortOrder, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'SortOrder=', IFNULL(OLD.SortOrder, 'NULL'), '
');
END IF;IF NEW.IconPlacementID != OLD.IconPlacementID OR
				( NEW.IconPlacementID IS NULL AND OLD.IconPlacementID IS NOT NULL) OR
				( NEW.IconPlacementID IS NOT NULL AND OLD.IconPlacementID IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'IconPlacementID=', IFNULL(NEW.IconPlacementID, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'IconPlacementID=', IFNULL(OLD.IconPlacementID, 'NULL'), '
');
END IF;IF NEW.ShiftDate != OLD.ShiftDate OR
				( NEW.ShiftDate IS NULL AND OLD.ShiftDate IS NOT NULL) OR
				( NEW.ShiftDate IS NOT NULL AND OLD.ShiftDate IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'ShiftDate=', IFNULL(NEW.ShiftDate, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'ShiftDate=', IFNULL(OLD.ShiftDate, 'NULL'), '
');
END IF;IF NEW.StartTime != OLD.StartTime OR
				( NEW.StartTime IS NULL AND OLD.StartTime IS NOT NULL) OR
				( NEW.StartTime IS NOT NULL AND OLD.StartTime IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'StartTime=', IFNULL(NEW.StartTime, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'StartTime=', IFNULL(OLD.StartTime, 'NULL'), '
');
END IF;IF NEW.EndTime != OLD.EndTime OR
				( NEW.EndTime IS NULL AND OLD.EndTime IS NOT NULL) OR
				( NEW.EndTime IS NOT NULL AND OLD.EndTime IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'EndTime=', IFNULL(NEW.EndTime, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'EndTime=', IFNULL(OLD.EndTime, 'NULL'), '
');
END IF;IF NEW.Enabled != OLD.Enabled OR
				( NEW.Enabled IS NULL AND OLD.Enabled IS NOT NULL) OR
				( NEW.Enabled IS NOT NULL AND OLD.Enabled IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'Enabled=', IFNULL(NEW.Enabled, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'Enabled=', IFNULL(OLD.Enabled, 'NULL'), '
');
END IF;IF newInfo!='' THEN INSERT INTO system_logs(Action,TableName,NewData,OldData,UserID,TableKeyID) VALUES('UPDATE','shifts', newInfo,oldInfo,IFNULL(@UserID,0),NEW.ID); END IF;
		END; //drop trigger if exists ad_shifts // CREATE TRIGGER ad_shifts AFTER DELETE ON shifts
				FOR EACH ROW
				BEGIN
					INSERT INTO system_logs(Action,TableName,oldData,UserID,TableKeyID)
		VALUES('DELETE','shifts', '',IFNULL(@UserID,0),OLD.ID);
		END; //drop trigger if exists ai_users // CREATE TRIGGER ai_users AFTER INSERT ON users
		FOR EACH ROW
		BEGIN
			DECLARE newInfo text;
			DECLARE oldInfo text;
			SET oldInfo = '';
			SET newInfo = CONCAT('','ID=',IFNULL(NEW.ID, 'NULL'), '
','Password_Hash=',IFNULL(NEW.Password_Hash, 'NULL'), '
','Username=',IFNULL(NEW.Username, 'NULL'), '
','CurrentEventID=',IFNULL(NEW.CurrentEventID, 'NULL'), '
'); INSERT INTO system_logs(Action,TableName,NewData,UserID,TableKeyID)
		VALUES('INSERT','users', newInfo,IFNULL(@UserID,0),NEW.ID);
		END; //
drop trigger if exists au_users // CREATE TRIGGER au_users AFTER UPDATE ON users
		FOR EACH ROW
		BEGIN
			DECLARE newInfo text;
			DECLARE oldInfo text;
			SET newInfo = '';
			SET oldInfo = '';IF NEW.ID != OLD.ID OR
				( NEW.ID IS NULL AND OLD.ID IS NOT NULL) OR
				( NEW.ID IS NOT NULL AND OLD.ID IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'ID=', IFNULL(NEW.ID, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'ID=', IFNULL(OLD.ID, 'NULL'), '
');
END IF;IF NEW.Password_Hash != OLD.Password_Hash OR
				( NEW.Password_Hash IS NULL AND OLD.Password_Hash IS NOT NULL) OR
				( NEW.Password_Hash IS NOT NULL AND OLD.Password_Hash IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'Password_Hash=', IFNULL(NEW.Password_Hash, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'Password_Hash=', IFNULL(OLD.Password_Hash, 'NULL'), '
');
END IF;IF NEW.Username != OLD.Username OR
				( NEW.Username IS NULL AND OLD.Username IS NOT NULL) OR
				( NEW.Username IS NOT NULL AND OLD.Username IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'Username=', IFNULL(NEW.Username, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'Username=', IFNULL(OLD.Username, 'NULL'), '
');
END IF;IF NEW.CurrentEventID != OLD.CurrentEventID OR
				( NEW.CurrentEventID IS NULL AND OLD.CurrentEventID IS NOT NULL) OR
				( NEW.CurrentEventID IS NOT NULL AND OLD.CurrentEventID IS NULL) THEN
					SET newInfo = CONCAT(newInfo,'CurrentEventID=', IFNULL(NEW.CurrentEventID, 'NULL'), '
');

					SET oldInfo = CONCAT(oldInfo,'CurrentEventID=', IFNULL(OLD.CurrentEventID, 'NULL'), '
');
END IF;IF newInfo!='' THEN INSERT INTO system_logs(Action,TableName,NewData,OldData,UserID,TableKeyID) VALUES('UPDATE','users', newInfo,oldInfo,IFNULL(@UserID,0),NEW.ID); END IF;
		END; //drop trigger if exists ad_users // CREATE TRIGGER ad_users AFTER DELETE ON users
				FOR EACH ROW
				BEGIN
					INSERT INTO system_logs(Action,TableName,oldData,UserID,TableKeyID)
		VALUES('DELETE','users', '',IFNULL(@UserID,0),OLD.ID);
		END; //
delimiter ;

alter table icon_templates add Prefix varchar(10) not null default '';
update icon_templates set Prefix = 'M' WHERE GroupType = 'Manager';   
update icon_templates set Prefix = 'S' WHERE GroupType = 'Supervisor';

CREATE TABLE user_permissions(
	`ID` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`EventID` INT(10) UNSIGNED NOT NULL,
	`UserID` INT(10) UNSIGNED NOT NULL,
	`CompanyID` INT(10) UNSIGNED NULL,
	AccessLevel ENUM('Readonly', 'Full', 'Admin'),
	PRIMARY KEY (`ID`),
	INDEX `FK__user_permissions_EventID` (`EventID`),
	INDEX `FK__user_permissions_CompanyID` (`CompanyID`),
	INDEX `FK__user_permissions_UserID` (`UserID`),
	CONSTRAINT `FK__user_permissions_EventID` FOREIGN KEY (`EventID`) REFERENCES `events` (`ID`) ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT `FK__user_permissions_CompanyID` FOREIGN KEY (`CompanyID`) REFERENCES `companies` (`ID`) ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT `FK__user_permissions_UserID` FOREIGN KEY (`UserID`) REFERENCES `users` (`ID`) ON UPDATE NO ACTION ON DELETE NO ACTION
)
ENGINE=InnoDB;

alter table events add COLUMN MapFolder VARCHAR(100) NOT NULL DEFAULT 'maptiles';

INSERT INTO user_permissions(EventID, UserID, CompanyID, AccessLevel)
SELECT e.ID, u.ID, null, 'Admin' FROM events e
INNER JOIN users u ON 1=1;



