-- SAMPLE DATA
INSERT INTO events(Name) VALUES('Coachella 2017');
INSERT INTO companies(Name, EventID) VALUES('Test Company', 1);
INSERT INTO users(Username, Password_Hash) VALUES('cam', '$2y$10$U.uFwy0YIWTFneBr2izNLuAeeYEI7GlLu74Mu3KlUN/UTre1eh8M6');
INSERT INTO company_skills(IconTemplateID, CompanyID, Name, RegularRate, OvertimeRate, OvertimeThreshold, DoubleTimeRate, DoubleTimeThreshold)
SELECT ID, 1, Name, 30, 45, 8, 60, 12
FROM icon_templates;

alter table users add CurrentEventID int(10) UNSIGNED not null default 1;
alter table events add CenterLat float(11,7) not null default 0, add CenterLng float(11,7) not null default 0;
ALTER TABLE users ADD CONSTRAINT fk_users_current_event_id FOREIGN KEY (CurrentEventID) REFERENCES events(ID);
insert into events(ID, Name, CenterLat, CenterLng) VALUES(2, 'FYF 2017', 34.014186, -118.287886);
update users set CurrentEventID = 2;

ALTER TABLE `companies`
	DROP INDEX `UIDX_companies_Name`,
	ADD UNIQUE INDEX `UIDX_companies_Name` (`EventID`, `Name`);

INSERT INTO companies(EventID, Name) SELECT 2, Name FROM companies WHERE EventID = 1;

INSERT INTO company_skills(IconTemplateID, CompanyID, Name, RegularRate, OvertimeRate, OvertimeThreshold, DoubleTimeRate, DoubleTimeThreshold) 
SELECT 
cs.IconTemplateID, cn.ID, cs.Name, cs.RegularRate, cs.OvertimeRate, cs.OvertimeThreshold, cs.DoubleTimeRate, cs.DoubleTimeThreshold
FROM company_skills cs
INNER JOIN companies co ON co.ID = cs.CompanyID AND co.EventID = 1
INNER JOIN companies cn ON cn.Name = co.Name AND cn.EventID = 2;


INSERT INTO user_permissions(EventID, UserID, CompanyID, AccessLevel)
SELECT e.ID, u.ID, null, 'Admin' FROM events e
INNER JOIN users u ON 1=1;

INSERT INTO icon_templates(Path, GroupType, Name) VALUES
	('ico/man_bike.png', 'Manager', 'Man. Bike'),
	('ico/man_horse.png', 'Manager', 'Man. Horse'),
	('ico/man_feet.png', 'Manager', 'Man. Post'),
	('ico/sup_bike.png', 'Supervisor', 'Sup. Bike'),
	('ico/sup_horse.png', 'Supervisor', 'Sup. Horse'),
	('ico/sup_feet.png', 'Supervisor', 'Sup. Post'),
	('ico/cs_bike.png', 'Post', 'Post Bike'),
	('ico/cs_horse.png', 'Post', 'Post Horse'),
	('ico/cs_feet.png', 'Post', 'Post');

	alter table icon_templates add Prefix varchar(10) not null default '';
update icon_templates set Prefix = 'M' WHERE GroupType = 'Manager';   
update icon_templates set Prefix = 'S' WHERE GroupType = 'Supervisor';