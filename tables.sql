CREATE DATABASE /*!32312 IF NOT EXISTS*/ `gv-scheduler`;
USE `gv-scheduler` ;

DROP TABLE IF EXISTS shifts;
DROP TABLE IF EXISTS icon_placements;
DROP TABLE IF EXISTS `company_skills`;
DROP TABLE IF EXISTS `icon_templates`;
DROP TABLE IF EXISTS `users`;
DROP TABLE IF EXISTS `companies`;
DROP TABLE IF EXISTS `events`;

CREATE TABLE `events` (
	ID INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Name VARCHAR(255),
	UNIQUE INDEX `UIDX_events_Name` (`Name`)
);

CREATE TABLE `companies` (
	ID INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	EventID INT UNSIGNED NOT NULL,
	Name VARCHAR(255),
	UNIQUE INDEX `UIDX_companies_Name` (`Name`),
	CONSTRAINT `FK__Event_Companies` FOREIGN KEY (`EventID`) REFERENCES `events` (`ID`) ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE `icon_templates` (
	ID INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Path VARCHAR(255),
	GroupType ENUM('Manager', 'Supervisor', 'Post') NOT NULL,
	Name VARCHAR(255)
);

INSERT INTO icon_templates(Path, GroupType, Name) VALUES
	('ico/man_bike.png', 'Manager', 'Man. Bike'),
	('ico/man_horse.png', 'Manager', 'Man. Horse'),
	('ico/man_feet.png', 'Manager', 'Man. Post'),
	('ico/sup_bike.png', 'Supervisor', 'Sup. Bike'),
	('ico/sup_horse.png', 'Supervisor', 'Sup. Horse'),
	('ico/sup_feet.png', 'Supervisor', 'Sup. Post'),
	('ico/cs_bike.png', 'Post', 'Post Bike'),
	('ico/cs_horse.png', 'Post', 'Post Horse'),
	('ico/cs_feet.png', 'Post', 'Post');
	
	
CREATE TABLE users (
	ID INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Password_Hash VARCHAR(255) NOT NULL,
	Username VARCHAR(255) NOT NULL,
	UNIQUE INDEX `UIDX_users_Username` (`Username`)
);


CREATE TABLE `company_skills` (
	ID INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	IconTemplateID INT(10) UNSIGNED NOT NULL,
	CompanyID INT(10) UNSIGNED NOT NULL,
	Name VARCHAR(255) NOT NULL,
	RegularRate FLOAT( 10, 6 ) NOT NULL, 
	OvertimeRate FLOAT( 10, 6 ) NOT NULL, 
	OvertimeThreshold FLOAT( 10, 6 ) NOT NULL, 
	DoubleTimeRate FLOAT( 10, 6 ) NOT NULL,
	DoubleTimeThreshold FLOAT( 10, 6 ) NOT NULL, 
	UNIQUE INDEX `UIDX_company_skills_Name` (`Name`, `CompanyID`),
	CONSTRAINT `FK__company_skills_Icons` FOREIGN KEY (`IconTemplateID`) REFERENCES `icon_templates` (`ID`) ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT `FK__company_skills_Companies` FOREIGN KEY (`CompanyID`) REFERENCES `companies` (`ID`) ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE `icon_placements` (
	ID INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	CompanySkillID INT(10) UNSIGNED NOT NULL,
	Lat FLOAT( 10, 6 ) NOT NULL,
	Lng FLOAT( 10, 6 ) NOT NULL,
	Description VARCHAR(100) NOT NULL,
	Callsign VARCHAR(100) NOT NULL,
	ParentPlacementID INT(10) UNSIGNED NULL,

	CONSTRAINT `FK__icon_placements_CompanySkill` FOREIGN KEY (`CompanySkillID`) REFERENCES `company_skills` (`ID`) ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT `FK__icon_placements_ParentPlacementID` FOREIGN KEY (`ParentPlacementID`) REFERENCES `icon_placements` (`ID`) ON UPDATE NO ACTION ON DELETE NO ACTION
);



CREATE TABLE `shifts` (
	ID INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	SortOrder INT(5) NOT NULL,
	IconPlacementID INT(10) UNSIGNED NOT NULL,
	ShiftDate DATE NOT NULL,
	StartTime Time NOT NULL,
	EndTime Time NOT NULL,
	Enabled INT(5) NOT NULL,
	CONSTRAINT `FK__shifts_IconPlacement` FOREIGN KEY (`IconPlacementID`) REFERENCES `icon_placements` (`ID`) ON UPDATE NO ACTION ON DELETE NO ACTION,
	UNIQUE INDEX `UIDX_shifts_IconPlacement_SortOrder` (`IconPlacementID`, ShiftDate, `SortOrder`)
);

/* Audit Tracking Changes */

CREATE TABLE `system_logs` (
	`OrderLogID` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`TableName` VARCHAR(50) NOT NULL DEFAULT '',
	`Action` ENUM('UPDATE','DELETE','INSERT') NOT NULL,
	`UserID` INT(10) NOT NULL,
	`TableKeyID` INT(10) NOT NULL,
	`OldData` TEXT NOT NULL,
	`NewData` TEXT NOT NULL,
	`ActionDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`OrderLogID`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

CREATE TABLE user_permissions(
	`ID` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`EventID` INT(10) UNSIGNED NOT NULL,
	`UserID` INT(10) UNSIGNED NOT NULL,
	`CompanyID` INT(10) UNSIGNED NULL,
	AccessLevel ENUM('Readonly', 'Full', 'Admin'),
	PRIMARY KEY (`ID`),
	INDEX `FK__user_permissions_EventID` (`EventID`),
	INDEX `FK__user_permissions_CompanyID` (`CompanyID`),
	INDEX `FK__user_permissions_UserID` (`UserID`),
	CONSTRAINT `FK__user_permissions_EventID` FOREIGN KEY (`EventID`) REFERENCES `events` (`ID`) ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT `FK__user_permissions_CompanyID` FOREIGN KEY (`CompanyID`) REFERENCES `companies` (`ID`) ON UPDATE NO ACTION ON DELETE NO ACTION,
	CONSTRAINT `FK__user_permissions_UserID` FOREIGN KEY (`UserID`) REFERENCES `users` (`ID`) ON UPDATE NO ACTION ON DELETE NO ACTION
)
ENGINE=InnoDB;

alter table events add COLUMN MapFolder VARCHAR(100) NOT NULL DEFAULT 'maptiles';